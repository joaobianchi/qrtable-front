'use strict'

const USER_SCHEMA = [
  'NAME', 'CITY',
  'ID', 'EMAIL', 'TOKEN',
  'PHONE', 'UF'
]

export const setUserInfo = (data) => USER_SCHEMA
  .forEach(key => localStorage.setItem(key, data[key.toLowerCase()]))

export const destroyUserInfo = () => USER_SCHEMA
  .forEach(key => localStorage.removeItem(key))

export const getUserInfo = () => {
  const user = []
  USER_SCHEMA.forEach(key => user[key.toLowerCase()] = localStorage.getItem(key))
  return user
}
