import axios from 'axios'
import * as manageLocalUserInfo from '../services/storage'

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
})

api.interceptors.request.use(
  config => {
    config.headers['Authorization'] = `Bearer ${manageLocalUserInfo.getUserInfo().token}`
    return config
  },
  error => {
    return Promise.reject(error)
  },
)

api.interceptors.response.use(
  response => {
    if (response.data.message === 'Logout efetuado com sucesso!') {
      window.location.reload()
    }
    return response
  },

  error => {
    if (error.response.data.message === 'Expired token') {
      alert('Sessão expirada. Necessário efetuar login novamente.')
      manageLocalUserInfo.destroyUserInfo()
      window.location.reload()
    }
    return error
  }
)

export default api
