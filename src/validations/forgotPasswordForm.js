import * as yup from 'yup'

const forgotPassword = yup.object({
  email: yup.string()
    .email('email informado não é válido')
    .required('necessário informar e-mail de recuperação de senha'),
});

export default forgotPassword