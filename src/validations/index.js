import login from './loginForm'
import forgotPassword from './forgotPasswordForm'
import registerUser from './registerUserForm'

export { login, forgotPassword, registerUser }