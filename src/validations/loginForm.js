import * as yup from 'yup'

const login = yup.object({
  email: yup.string()
    .email('email informado não é válido')
    .required('necessário informar e-mail de login'),
  password: yup.string()
    .min(8, 'senha deve ter no mínimo 8 caracteres')
    .max(30, 'senha deve ter no máximo 30 caracteres')
    .required('necessário informar senha de login'),
});

export default login