import * as yup from 'yup'

const registerUser = yup.object({
  name: yup.string().required('nome é obrigatório.'),
  email: yup.string()
    .email('email informado não é válido')
    .required('necessário informar e-mail de login'),
  phone: yup.string().required('telefone para contato é obrigatório'),
  city: yup.string().required('cidade é obrigatório.'),
  uf: yup.string().required('estado é obrigatório.'),
  password: yup.string().min(8, 'senha muito curta').required('senha é obrigatório.'),
  confirmPassword: yup.string().min(8, 'senha muito curta').required('confirmar senha é obrigatório.'),
});

export default registerUser