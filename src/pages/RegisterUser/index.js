import React, { useMemo, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Card, Form, Container, Row, Col, Button } from 'react-bootstrap';
import { Password, InputText, Select, InputTextMask } from '../../components/Form/'
import { Formik } from 'formik';
import * as fireToast from '../../utils/fireToast';
import useActions from '../../store/ducks/sendRequest'
import { registerUser } from '../../validations'
import { citiesStatesBrazil, httpStatus } from '../../miscellaneous'

const RegisterUser = () => {

  const [redirectToLogin, setRedirectToLogin] = useState(false)
  const [selectedUF, setSelectedUF] = useState('AC')
  const [selectedUFCities, setSelectedUFCities] = useState(
    citiesStatesBrazil[0].cities.map(city => {
      return { title: city, value: city }
    })
  )

  const {
    error,
    success,
    sendRequest,
  } = useActions();

  const initialValueForm = {
    name: '',
    email: '',
    password: '',
    phone: '',
    uf: '',
    city: '',
    password: '',
    confirmPassword: ''
  }

  const buildDataRegister = data => {
    const user = {}

    user.name = data.name
    user.email = data.email
    user.phone = data.phone.replace(/[\.() ,:-]+/g, '')
    user.uf = data.uf
    user.city = data.city
    user.password = data.password

    return user
  }

  const handleSubmit = data => {

    if (data.confirmPassword !== data.password) {
      fireToast.error('Senhas não conferem. Favor verificar.')
    } else {
      const buildedDataRegisterUser = buildDataRegister(data)

      sendRequest('/user', 'POST', 'GENERAL_REQUEST', buildedDataRegisterUser)
        .then(result => result.status && result.status === httpStatus.SUCCESS && setRedirectToLogin(true))
    }
  }

  const handleChangeUF = uf => {
    setSelectedUF(uf)
    let cities = []
    citiesStatesBrazil.map(state => state.value === uf && state.cities
      .map(city => cities.push({ title: city, value: city })))
    setSelectedUFCities(cities)
  }

  const renderCardRegisterUserBody = useMemo(() => {
    return (
      <Card.Body>
        <Formik
          initialValues={initialValueForm}
          validationSchema={registerUser}
          onSubmit={handleSubmit}
        >{({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          errors,
        }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Group>
                <InputText
                  label="nome completo:"
                  name="name"
                  type="text"
                  placeholder="Ex: John Ray Cash"
                  onBlur={handleBlur}
                  value={values.name}
                  touched={touched.name}
                  errors={errors.name}
                  onChange={handleChange}
                />
                <InputText
                  label="email:"
                  name="email"
                  type="email"
                  placeholder="Ex: max_rockatansky@mad.com"
                  onBlur={handleBlur}
                  value={values.email}
                  touched={touched.email}
                  errors={errors.email}
                  onChange={handleChange}
                />
                <InputTextMask
                  label="telefone celular:"
                  name="phone"
                  type="text"
                  placeholder="Ex: (21) 2 9704-1330"
                  onBlur={handleBlur}
                  value={values.phone}
                  touched={touched.phone}
                  errors={errors.phone}
                  onChange={handleChange}
                />
                <Form.Row>
                  <Col lg={4}>
                    <Select
                      label="estado:"
                      name="uf"
                      selected={selectedUF}
                      as="select"
                      onChange={(e) => {
                        handleChangeUF(e.target.value)
                        handleChange(e)
                      }}
                      options={citiesStatesBrazil}
                      value={values.uf}
                      touched={touched.uf}
                      errors={errors.uf}
                    />
                  </Col>
                  <Col lg={8}>
                    <Select
                      label="cidade:"
                      name="city"
                      as="select"
                      onChange={handleChange}
                      options={selectedUFCities}
                      value={values.city}
                      touched={touched.city}
                      errors={errors.city}
                    />
                  </Col>
                </Form.Row>
                <Password
                  name="password"
                  label="senha:"
                  placeholder="*********"
                  value={values.password}
                  onChange={handleChange}
                  touched={touched.password}
                  errors={errors.password}
                />
                <Password
                  name="confirmPassword"
                  label="confirmar senha:"
                  placeholder="*********"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  touched={touched.password}
                  errors={errors.password}
                />
                <Form.Text className="text-muted">escolha bem sua senha, ela faz parte da nossa segurança (:</Form.Text>
              </Form.Group>
              <Button variant="primary" type="sumbit" className="btn-block">registrar</Button>
            </Form>
          )}
        </Formik>
      </Card.Body>
    )
  }, [selectedUFCities])

  useEffect(() => {
    console.log(error, '++++++', success)
    if (error && error.response.data.message !== '')
      fireToast.error(error.response.data.message)
    if (success && success.message !== '')
      fireToast.success(success.message)
    return () => {
      if (success)
        success.message = ''
      if (error)
        error.response.data.message = ''
    }
  }, [success, error])

  return (
    redirectToLogin ? (<Redirect to="/login" />) :
      (
        <Container fluid>
          <Row className="flex-column h-100 align-content-center">
            <Col sm={10} md={6} lg={4}>
              <div style={{ marginTop: '5%' }}>
                <h2 className="text-center">qrTable</h2>
                <p className="text-center">Facilite o pedido do seu cliente.</p>
                <Card>
                  {renderCardRegisterUserBody}
                  <div className="text-center mb-1">
                    <a href="/login">voltar para login</a>
                  </div>
                </Card>
              </div>
              <div className="text-center mt-1">
                <p className="pr-5 pl-5">Ao se registrar, você concorda com os{' '}
                  <a href="/register-user" className="text-center">termos e condições</a>
                  {' '}dos serviços.</p>
              </div>
            </Col>
          </Row>
        </Container>
      )
  );
};

export default RegisterUser;
