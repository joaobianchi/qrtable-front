import React, { useMemo } from 'react';
import { Card, Form, Container, Row, Col, Button } from 'react-bootstrap';
import { Password, InputText } from '../../components/Form/'
import { Formik } from 'formik';
import { login } from '../../validations'

const Login = () => {

  const emailUser = localStorage.getItem('EMAIL_USER')
  const remember = localStorage.getItem('REMEMBER_ME')

  const initialValueForm = {
    email: emailUser ? emailUser : '',
    password: '',
    rememberMe: remember ? true : false
  }

  const verificationRememberMe = data => {
    if (data.rememberMe) {
      localStorage.setItem('REMEMBER_ME', true)
      localStorage.setItem('EMAIL_USER', data.email)
    } else {
      localStorage.removeItem('REMEMBER_ME')
      localStorage.removeItem('EMAIL_USER')
    }
  }

  const handleSubmit = data => {
    verificationRememberMe(data)
    console.log(data, 'DATA')
  }

  const renderCardLoginBody = useMemo(() => {
    return (
      <Card.Body>
        <Formik
          initialValues={initialValueForm}
          validationSchema={login}
          onSubmit={handleSubmit}
        >{({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          errors,
        }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Group>
                <Form.Label>e-mail:</Form.Label>
                <InputText
                  name="email"
                  type="email"
                  placeholder="digite aqui seu e-mail"
                  onBlur={handleBlur}
                  value={values.email}
                  touched={touched.email}
                  errors={errors.email}
                  onChange={handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>senha:</Form.Label>
                <Password
                  placeholder="digite aqui sua senha"
                  value={values.password}
                  onChange={handleChange}
                  touched={touched.password}
                  errors={errors.password}
                />
                <Form.Text className="text-muted">mantenha sua senha atualizada (:</Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicCheckbox">
                <Form.Check
                  name="rememberMe"
                  type="switch"
                  value={values.rememberMe}
                  defaultChecked={values.rememberMe}
                  onChange={handleChange}
                  label="lembrar-me"
                  className="small"
                />
              </Form.Group>
              <Button variant="primary" type="sumbit" className="btn-block">login</Button>
            </Form>
          )}
        </Formik>
      </Card.Body>
    )
  }, [])

  return (
    (
      <Container fluid>
        <Row className="flex-column h-100 align-content-center">
          <Col sm={10} md={5} lg={3}>
            <div style={{ marginTop: '25%' }}>
              <h2 className="text-center">Qr Table</h2>
              <p className="text-center">Facilite o pedido do seu cliente.</p>
              <Card>
                {renderCardLoginBody}
                <div className="text-center mb-1">
                  <a href="/forgot-my-password">esqueci minha senha</a>
                </div>
              </Card>
            </div>
            <div className="text-center mt-1">
              <p>Não possui uma conta? Registre-se <a href="/register-user" className="text-center">aqui.</a> É grátis.</p>
            </div>
          </Col>
        </Row>
      </Container>
    )
  );
};

export default Login;
