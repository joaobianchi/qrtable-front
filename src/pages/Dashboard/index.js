import React from 'react';
import { Container } from 'react-bootstrap';
import Header from '../../components/Header'
import Sidebar from '../../components/Sidebar'

const Dashboard = ({ history }) => {

  return (
    (
      <>
        <Header />
        <Sidebar history={history} />
        <Container fluid>
        </Container>
      </>
    )
  );
};

export default Dashboard;
