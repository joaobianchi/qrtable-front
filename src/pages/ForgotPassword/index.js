import React, { useMemo } from 'react';
import { Card, Container, Row, Col, Form, Button } from 'react-bootstrap';
import { InputText } from '../../components/Form/'
import { Formik } from 'formik';
import { forgotPassword } from '../../validations'

const ForgotPassword = () => {

  const handleSubmit = data => {
    console.log(data, 'DATA')
  }

  const renderCardForgotMyPassword = useMemo(() => {
    return (
      <Card.Body>
        <Formik
          initialValues={{ email: '' }}
          validationSchema={forgotPassword}
          onSubmit={handleSubmit}
        >{({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          errors,
        }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Group>
                <Form.Label>e-mail:</Form.Label>
                <InputText
                  name="email"
                  type="email"
                  placeholder="digite aqui seu e-mail"
                  onBlur={handleBlur}
                  value={values.email}
                  touched={touched.email}
                  errors={errors.email}
                  onChange={handleChange}
                />
              </Form.Group>
              <Button variant="primary" type="sumbit" className="btn-block">enviar</Button>
            </Form>
          )}
        </Formik>
      </Card.Body>
    )
  }, [])

  return (
    (
      <Container fluid>
        <Row className="flex-column h-100 align-content-center">
          <Col sm={12} className="w-25">
            <div style={{ marginTop: '30%' }}>
              <h2 className="text-center">Qr Table</h2>
              <p className="text-center mb-3">Facilite o pedido do seu cliente.</p>
              <Card>
                <Card.Header className="text-center">recuperar senha</Card.Header>
                {renderCardForgotMyPassword}
                <div className="text-center mb-1">
                  <a href="/login">voltar para login</a>
                </div>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    )
  );
};

export default ForgotPassword;
