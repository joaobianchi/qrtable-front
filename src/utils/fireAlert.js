import Swal from 'sweetalert2'

const Alert = Swal.mixin({
  showCloseButton: true,
  showCancelButton: true,
  cancelButtonColor: '#2D77C1',
  confirmButtonColor: '#d33',
  cancelButtonText:
    '<i class="fa fa-thumbs-down"></i> Não!',
  confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> Sim!',
})

export function question(question, content, url) {
  let result = Alert.fire({ title: question, text: content })
    .then((result) => {
      const setUrl = url

      if (!setUrl)
        return result.value

      if (result.value && setUrl)
        document.location.replace(url)
    })
  return result
}
