import Swal from 'sweetalert2'

const Toast = Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 4000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

export function success(message) {
  Toast.fire({ icon: 'success', title: message })
}

export function error(message) {
  Toast.fire({ icon: 'error', title: message })
}

export function warning(message) {
  Toast.fire({ icon: 'warning', title: message })
}
