import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faUserAlt } from '@fortawesome/free-solid-svg-icons'
import { Navbar, NavbarBrand, Nav, NavItem, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import './style.css'

export default function Header({ ...rest }) {

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen(prevState => !prevState);

  function openNav() {
    document.getElementById('sidebar-collapse').style.width = '215px';
    document.getElementById('burguer-menu').style.visibility = 'hidden';
    document.getElementById('root').style.marginLeft = '215px';
  }

  return (
    <>
      <Navbar className="navbar navbar-expand navbar-white navbar-light border-bottom" color="faded" light>
        <Nav className="d-flex justify-content-between align-items-center w-100" navbar>
          <NavItem id="burguer-menu" className="burguer-menu openbtn" onClick={openNav} >
            <FontAwesomeIcon icon={faBars} size="lg" color="dark" />
          </NavItem>
          <NavItem>
            <NavbarBrand href="/" className="mr-auto">qrTable</NavbarBrand>
          </NavItem>
          <NavItem>
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret color="white" className="d-flex align-items-center p-0">
                <div className="p-1 border rounded-circle avatar-dropdown">
                  <FontAwesomeIcon icon={faUserAlt} size="md" color="grey" />
                </div>
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem header>minha conta</DropdownItem>
                <DropdownItem>editar perfil</DropdownItem>
                <DropdownItem>configurações de conta</DropdownItem>
                <DropdownItem divider />
                <DropdownItem header>configurações</DropdownItem>
                <DropdownItem>geral</DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </NavItem>
        </Nav>
      </Navbar>
    </>
  );
}
