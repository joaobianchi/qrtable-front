import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function MenuItem({ ...props }) {

  return (
    <>
      <div className="menu-item p-1">
        <a
          data-widget="pushmenu"
          className={`nav-link ${props.className} `}
          onClick={props.onClick}
        >
          {props.text && props.icon && !props.isRight && (<FontAwesomeIcon icon={props.icon} className="nav-icon mr-3" size="md" color="dark" />)}
          {!props.text && props.icon && (<FontAwesomeIcon icon={props.icon} className="nav-icon mr-2" size="md" color="dark" />)}
          <span>{props.text}</span>
          {props.isRight && props.icon && (<FontAwesomeIcon icon={props.icon} className="nav-icon ml-2" size="md" color="dark" />)}
        </a>
      </div>
    </>
  )
}

export default MenuItem
