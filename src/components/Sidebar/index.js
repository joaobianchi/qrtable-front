import React from 'react'
//import { getUserInfo, destroyUserInfo } from '../../services/storage'
import * as fireAlert from '../../utils/fireAlert'
import { httpStatus } from '../../miscellaneous'
import * as fireToast from '../../utils/fireToast'
import { faTachometerAlt, faUtensils, faQrcode, faMobileAlt, faUserAlt } from '@fortawesome/free-solid-svg-icons'
import { MenuItem } from '../NavMenu'
import './style.css'

const Sidebar = ({ history }) => {
  let path = history.location.pathname

  const paths = {
    DASHBOARD: 'dashboard',
    TABLES: 'tables',
    MENU: 'menu',
    MOBILE: 'mobile-numbers',
  }

  /* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
  function closeNav() {
    document.getElementById('sidebar-collapse').style.width = "0";
    document.getElementById('root').style.marginLeft = "0";
    document.getElementById('burguer-menu').style.visibility = 'initial';
  }

  return (
    <aside className="sidebar dark" id="sidebar-collapse">
      <a href="javascript:void(0)" class="closebtn" onClick={closeNav}>&times;</a>
      <div className="pr-3 pl-3 text-center">
        <hr />
        <a href="/dashboard" className="brand-link">
          qrTable
      </a>
        <hr />
      </div>
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <MenuItem
          icon={faTachometerAlt}
          text="dashboard"
          className={`nav-link ${path.includes(paths.DASHBOARD) ? 'active' : ''}`}
          onClick={() => history.push('/dashboard')}
        />
        <MenuItem
          icon={faQrcode}
          text="mesas"
          className={`nav-link ${path.includes(paths.TABLES) ? 'active' : ''}`}
          onClick={() => history.push('/tables')}
        />
        <MenuItem
          icon={faUtensils}
          text="menu"
          className={`nav-link ${path.includes(paths.MENU) ? 'active' : ''}`}
          onClick={() => history.push('/tables')}
        />
        <MenuItem
          icon={faMobileAlt}
          text="celulares"
          className={`nav-link ${path.includes(paths.MOBILE) ? 'active' : ''}`}
          onClick={() => history.push('/mobile-numbers')}
        />
        <MenuItem
          icon={faUserAlt}
          text="usuários"
          className={`nav-link ${path.includes(paths.MOBILE) ? 'active' : ''}`}
          onClick={() => history.push('/mobile-numbers')}
        />
      </div>
    </aside>
  )
}

export default Sidebar
