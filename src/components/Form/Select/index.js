import React from 'react';
import { Form } from 'react-bootstrap';

const Select = ({ ...rest }) => {

  return (
    <>
      <Form.Label>{rest.label}</Form.Label>
      <Form.Control
        as="select"
        className={`mb-2 ${rest.touched && rest.errors ? "error" : null}`}
        value={rest.values}
        {...rest}
      >
        {rest.options.length > 0 && rest.options.map(option => (
          <option value={option.value}>{option.title}</option>
        ))}
      </Form.Control>
      {rest.touched && rest.errors ? (
        <div>
          <small className="error-message">{rest.errors}</small>
        </div>
      ) : null}
    </>
  )
}

export default Select