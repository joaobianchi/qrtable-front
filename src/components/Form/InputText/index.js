import React from 'react';
import { Form } from 'react-bootstrap';

const InputText = ({ ...rest }) => {

  return (
    <>
      <Form.Label>{rest.label}</Form.Label>
      <Form.Control
        className={`mb-2 ${rest.touched && rest.errors ? "error" : null}`}
        {...rest}
      />
      {rest.touched && rest.errors ? (
        <div className="error-message">
          <small>{rest.errors}</small>
        </div>
      ) : null}
    </>
  )
}

export default InputText