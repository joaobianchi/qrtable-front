import React from 'react'
import { Form } from 'react-bootstrap'

const Password = ({ ...rest }) => {

  return (
    <>
      <Form.Label>{rest.label}</Form.Label>
      <Form.Control
        name="password"
        type="password"
        className={`mb-2 ${rest.touched && rest.errors ? "error" : null}`}
        {...rest}
      />
      {rest.touched && rest.errors ? (
        <div>
          <small className="error-message">{rest.errors}</small>
        </div>
      ) : null}
    </>
  )
}

export default Password