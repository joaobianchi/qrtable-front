import React from 'react';
import MaskedInput from "react-maskedinput";
import { Form } from 'react-bootstrap';

const InputTextMask = ({ ...rest }) => {

  return (
    <>
      <Form.Label>{rest.label}</Form.Label>
      <MaskedInput
        className={`form-control mb-2 ${rest.touched && rest.errors ? "error" : null}`}
        mask="(11) 1 1111-1111"
        placeholder="Ex: (67) 8 1367-0920"
        size="14"
        {...rest}
      />
      {rest.touched && rest.errors ? (
        <div className="error-message">
          <small>{rest.errors}</small>
        </div>
      ) : null}
    </>
  )
}

export default InputTextMask