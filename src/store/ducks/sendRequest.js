import { useReducer, useCallback } from 'react';
import api from '../../services/api';
import { httpStatus } from '../../miscellaneous';

const initialDataState = {
  response: {},
  error: null,
  success: null
}

const actionReducer = (actionsState, action) => {
  switch (action.type) {
    case 'GENERAL_REQUEST':
      return { ...actionsState, response: action.data };
    case 'SUCCESS':
      return { ...actionsState, success: action.data }
    case 'ERROR':
      return { ...actionsState, error: action.data }
    default:
      return { ...actionsState, error: action.data };
  }
};

const useActions = () => {
  const [actionsState, dispatchAction] = useReducer(actionReducer, initialDataState);

  const sendRequest = useCallback(async (url, method, type = '', data = []) => {
    return api({ method: method, url: url, data: data })
      .then(resultData => {
        if (resultData.response && resultData.response.status !== httpStatus.SUCCESS) {
          dispatchAction({ type: 'ERROR', data: resultData })
        }

        if (resultData.status && resultData.status === httpStatus.SUCCESS) {
          dispatchAction({ type: type, data: resultData })

          if (method !== 'GET')
            dispatchAction({ type: 'SUCCESS', data: resultData.data })

        }

        return resultData
      })
      .catch(error => dispatchAction({ type: 'ERROR', data: { message: 'Algo deu errado, por gentileza atualize a página.' } }))
  }, [])

  return {
    response: actionsState.response,
    error: actionsState.error,
    success: actionsState.success,
    sendRequest
  };
};

export default useActions;
